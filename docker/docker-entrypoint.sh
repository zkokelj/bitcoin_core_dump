#!/bin/bash
set -e

echo "TEST BITCOINSV - Ziga Kokelj!"

TEST=$(cat /proc/sys/kernel/core_pattern)
echo "Core dump will be generated in: $TEST"
echo "--------------------------------------"

firstChar="$(echo $TEST | head -c 1)"

# It automatically splits string to string array
stringarray=($TEST)

if [[ $firstChar == "|" ]]; then
	echo "We need to rewrite script!"
	SCRIPT=${stringarray[0]}

	# remove first character (pipe)
	SCRIPT="${SCRIPT:1}"
	echo "The script is: $SCRIPT"

else
	echo "We want to write to directory - create it if not yet created"
	ulimit -c
	directory=`dirname "$TEST"`
	echo "Directory: $directory"
	mkdir -p "$directory"
	chmod 777 "$directory"
fi

echo "Parameters are: $@"
ls /usr/local/bin
echo "-------------"

# exit 0

# Now run bitcoin...

if [[ "$1" == "bitcoin-cli" || "$1" == "bitcoin-tx" || "$1" == "bitcoind" || "$1" == "test_bitcoin" ]]; then
	mkdir -p "$BITCOIN_DATA"

	if [[ ! -s "$BITCOIN_DATA/bitcoin.conf" ]]; then
		cat <<-EOF > "$BITCOIN_DATA/bitcoin.conf"
		printtoconsole=1
		rpcallowip=::/0
		rpcpassword=${BITCOIN_RPC_PASSWORD:-password}
		rpcuser=${BITCOIN_RPC_USER:-bitcoin}
		EOF
		chown bitcoin:bitcoin "$BITCOIN_DATA/bitcoin.conf"
	fi

	# ensure correct ownership and linking of data directory
	# we do not update group ownership here, in case users want to mount
	# a host directory and still retain access to it
	chown -R bitcoin "$BITCOIN_DATA"
	ln -sfn "$BITCOIN_DATA" /home/bitcoin/.bitcoin
	chown -h bitcoin:bitcoin /home/bitcoin/.bitcoin

    echo $PATH
	exec gosu bitcoin "$@"
fi

exec "$@"
